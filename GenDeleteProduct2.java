import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.StringJoiner;

public class GenDeleteProduct2 {

	public static void main(String[] args) throws IOException {
		String homeDir = System.getProperty("user.home");
		String fileName = homeDir + "/opt/gen_sql/gwp_3source_20200420.csv";
		String outputName = homeDir + "/opt/gen_sql/gwp_3source_20200420_output.txt";
		File file = new File(fileName);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		String[] str = null;
		int index = 0;
		String bu, ibc, storeStock;
		Hashtable<String, StringJoiner> ht = new Hashtable<>();

		while ((line = br.readLine()) != null) {
			if (index != 0) {
				line = line.replaceAll("\\s", "").trim();
				// System.out.println(line);

				str = line.split(",");

				// System.out.println(str[0] + "   " + str[1]);
				

				bu = str[0].trim().substring(0, 3);
				ibc = str[0].trim().substring(3, str[0].length());
				storeStock = str[1];


				String[] ss = storeStock.split("and");
				// System.out.println(bu + " " + ibc + "|" + ss[0]);
				// System.out.println(bu + " " + ibc + "|" + ss[1]);


				// if(ibc.equals("10547755") || ibc.equals("95167923")){
				// 	System.out.println("Found : " + ibc);
				// }

				for(int i=0 ; i<ss.length;i++){
					String key = bu + ss[i];
					key = key.trim();

					System.out.println(key);

					if (ht.containsKey(key)) {
						StringJoiner joiner = ht.get(key);
						joiner.add("'" + ibc + "'\n");
						// System.out.println(tmp.toString());
						ht.put(key, joiner);
					} else {
						StringJoiner joiner = new StringJoiner(", ");
						joiner.add("'" + ibc + "'\n");
						ht.put(key, joiner);
					}

				}

				// String key = bu + storeStock;
				// key = key.trim();

				

				// if (ht.containsKey(key)) {
				// 	StringJoiner joiner = ht.get(key);
				// 	joiner.add("'"+ibc+"'\n");
				// 	// System.out.println(tmp.toString());
				// 	ht.put(key, joiner);
				// } else {
				// 	StringJoiner joiner = new StringJoiner(", ");
				// 	joiner.add("'"+ibc+"'\n");
				// 	ht.put(key, joiner);
				// }
			}
			index++;
			// if(index == 2){
			// break;
			// }
		}

		br.close();


		BufferedWriter writer = new BufferedWriter(new FileWriter(outputName));

		ht.forEach((k, v) -> {
			// System.out.println("Item : " + k + " Count : " + v.toString());
			try {

				String s = "DELETE FROM ENABLE_PRODUCT WHERE BU='" + k.substring(0, 3) + "' AND IBC IN (" + v.toString()
						+ ") AND STORE_STOCK = '" + k.substring(3, k.length()) + "';";
				writer.write(s + "\n\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		writer.close();

	}

}
