import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.StringJoiner;

public class GenDeleteProduct {

	public static void main(String[] args) throws IOException {
		String fileName = "/Users/itsaret/opt/gen_sql/input.csv";
		String outputName = "/Users/itsaret/opt/gen_sql/output.txt";
		File file = new File(fileName);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		String[] str = null;
		int index = 0;
		String bu, ibc, storeStock;
		Hashtable<String, StringJoiner> ht = new Hashtable<>();
		while ((line = br.readLine()) != null) {
			if (index != 0) {
				// System.out.println(line);
				str = line.split(",");
				bu = str[1].substring(0, 3);
				ibc = str[1].substring(3, str[1].length());
				storeStock = str[2];

				// System.out.println(bu + " " + ibc + " " + storeStock);

				if(ibc.equals("10547755") || ibc.equals("95167923")){
					System.out.println("Found : " + ibc);
				}

				String key = bu + storeStock;
				key = key.trim();

				if (ht.containsKey(key)) {

					if(ibc.equals("10547755") || ibc.equals("95167923")){
						System.out.println("Join : " + ibc);
					}

					StringJoiner joiner = ht.get(key);
					joiner.add("'"+ibc+"'\n");
					// System.out.println(tmp.toString());
					ht.put(key, joiner);
				} else {

					if(ibc.equals("10547755") || ibc.equals("95167923")){
						System.out.println("Join new : " + ibc);
					}

					// System.out.println(key);
					StringJoiner joiner = new StringJoiner(", ");
					joiner.add("'"+ibc+"'\n");
					ht.put(key, joiner);
				}
			}
			index++;
			// if(index == 2){
			// break;
			// }
		}

		br.close();

		// System.out.println(ht);

		BufferedWriter writer = new BufferedWriter(new FileWriter(outputName));

		ht.forEach((k,v)->{
			// System.out.println("Item : " + k + " Count : " + v.toString());
			try {

				String s ="DELETE FROM ENABLE_PRODUCT WHERE BU='"+k.substring(0,3)+"' AND IBC IN ("+v.toString()+") AND STORE_STOCK = '"+k.substring(3,k.length())+"';";
				writer.write(s + "\n\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// if("E".equals(k)){
			// 	System.out.println("Hello E");
			// }
		});

		writer.close();

	}

}
